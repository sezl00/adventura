/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/*******************************************************************************
 * Instance třídy Vec pracuje s věcí. Řeší, zda věc lze sebrat nebo ne.
 *
 * @author    Lukáš Sezima
 * @version   rok 2017/2018
 */
public class Vec
{
    
private String nazev;
private boolean prenositelnost;
/***************************************************************************
*  Konstruktor ....
*/
    public Vec(String nazev,boolean prenositelnost)
    {
        this.nazev = nazev;
        this.prenositelnost = prenositelnost;
    }


    /**Metoda, která zjišťuje název věci.
     * @return vrací název
     * 
     */
    public String getNazev(){
    return nazev;
}

/*********
 * Metoda, která zjišťuje, zda-li je věc přenositelná.
 * @return je přenositelná
 */
public boolean isPrenositelnost(){
    return prenositelnost;
}


}
