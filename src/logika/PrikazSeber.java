/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;
import java.util.*;



/*******************************************************************************
 * Instance třídy PrikazSeber implenetuje pro hru příkaz seber.
 *
 * @author    Lukáš Sezima
 * @version   rok 2017/2018
 */
public class PrikazSeber implements IPrikaz
{
   private static final String NAZEV = "seber";
   private HerniPlan plan;
   private Paluba paluba;

    /**
    *  Konstruktor třídy
    *
    *  @param plan herní plán, ve kterém se bude ve hře něco "sbírat"
    */
    public PrikazSeber(HerniPlan plan, Paluba paluba) {
        this.plan = plan;
        this.paluba = paluba;
    }

    /**
     *  Provádí příkaz "seber". Zkouší něco sebrat ze zadaného prostoru. Pokud předkět lze sebrat
     *  sebere se to. Pokud to sebrat nelze
     *  vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno věci,
     *                         kterou sebrat.
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Co mám sebrat? Musíš zadat název věci.";
        }

        String nazevSbiraneVeci = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor aktualniProstor = plan.getAktualniProstor();
        Vec sbirana = aktualniProstor.odeberVec(nazevSbiraneVeci);     //zjišťuji, jestli je daná věc v místnosti, zjistím, jestli ji lze z prostoru vyjmout

        if (sbirana == null) {
            return "To tu není!";
        }
        else {
            if(sbirana.isPrenositelnost()){           //ptám se, jestli je věc lze zvednout nebo ne
                if (sbirana.getNazev().equals("ryby")) {
                  aktualniProstor.vlozVec(sbirana);  
                }
                else {
                if (sbirana.getNazev().equals("ovoce")) {
                  aktualniProstor.vlozVec(sbirana);
                }
                else {
                    if (sbirana.getNazev().equals("zlato")) {
                        aktualniProstor.vlozVec(sbirana);
                    }          
                }
            }
            
                if(paluba.vloz(sbirana)) {
                  return "Sebral si " + nazevSbiraneVeci ;  //pokud se to do batohu vložilo, vypíše, co se vložilo
              }

              //pokud se to do batohu nevložilo, tak to vraťme zpět do prostoru
              if (!sbirana.getNazev().equals("ryby")) {
                // když to co, sbírám, ale nepovedlo se to, jsou ryby
                //tak je nikdy nesmím vrátit zpátky, aby tam nebyli vícekrát
                aktualniProstor.vlozVec(sbirana);
              }
              return "Tohle už se ti na palubu nevejde.";
            } else{             
             if (sbirana.getNazev().equals("ryby")) {
              if (!sbirana.getNazev().equals("ovoce")){  
                aktualniProstor.vlozVec(sbirana);           //vrátím věc zpátky do prostoru       
                }               
             }            
             else {
                aktualniProstor.vlozVec(sbirana);         //vrátím věc zpátky do prostoru
            }
             return "Toto opravdu neuzvedneš!";
            } 
        
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

   }
