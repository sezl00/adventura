/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kodovani: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/**
 * Instance třídy PrikazKolik implementuje pro hru příkaz proveď.
 *
 * @author Lukáš Sezima
 * @version rok 2017/2018
 */
public class PrikazKolik implements IPrikaz
{
    private static final String NAZEV = "kolik";
    private Paluba paluba;
    
    /**
    *  Konstruktor třídy
    *
    *  @param paluba, bude se vypisovat obsah paluby, hodnoty, kolik je volno a kolik obsazeno
    */
    public PrikazKolik(Paluba paluba) {
        this.paluba=paluba;
    }
    
     @Override
    public String provedPrikaz(String... parametry) {    
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            String kolik = "Na palubě je ";
            kolik += paluba.pocet();
            kolik += " míst obsazeno a ";
            kolik += (7 - paluba.pocet());
            kolik += " míst volných.";
            return kolik;
        }
        return "Promiň, nevím, co mám ukázat.";
    }
    
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
