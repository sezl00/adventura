package logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Lukáš Sezima
 *@version    rok 2017/2018
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    private Paluba paluba;
    private Hra hra;

    /**
    *  Konstruktor třídy
    *
    *  @param plan herní plán, ve kterém se bude ve hře "chodit"
    */
    public PrikazJdi(HerniPlan plan, Paluba paluba, Hra hra) {
        this.plan = plan;
        this.paluba = paluba;
        this.hra = hra;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null) {
            return "Tam se odtud jít nedá!";
        }

        if (sousedniProstor.equals(new Prostor("doky", "doky. To je tvůj cíl."))){  //ZIMMIK existují i lepší způsoby jak zakázat přístup, pokud nemáš nějakou věc, al etohle je s prominutím pro retardy, takže to asi bude to co chcete, jenom bahca, ten prostor se musí přesně schodovat, jinak to nebude fungovat
          if (paluba.jeNaPalube("zlato")){
            plan.setAktualniProstor(sousedniProstor);
            hra.setKonecHry(true);
            return "";//sousedniProstor.dlouhyPopis();
          } else {
            return "Nemáš věci pro přístup. Potřebuješ zlato.";
          }
        }
        if (sousedniProstor.equals(new Prostor("průplav", "Jsi v oblasti průplav."))){  //ZIMMIK existují i lepší způsoby jak zakázat přístup, pokud nemáš nějakou věc, al etohle je s prominutím pro retardy, takže to asi bude to co chcete, jenom bahca, ten prostor se musí přesně schodovat, jinak to nebude fungovat
          if (paluba.jeNaPalube("ovoce")){
            plan.setAktualniProstor(sousedniProstor);
            return sousedniProstor.dlouhyPopis();
          } else {
            return "Nemáš věci pro přístup. Potřebuješ bednu s ovocem.";
          }
        }
        if (sousedniProstor.equals(new Prostor("neznámá_oblast", "Jsi v neznámé oblasti ."))){  //ZIMMIK existují i lepší způsoby jak zakázat přístup, pokud nemáš nějakou věc, al etohle je s prominutím pro retardy, takže to asi bude to co chcete, jenom bahca, ten prostor se musí přesně schodovat, jinak to nebude fungovat
          if (paluba.jeNaPalube("mapa")){
            plan.setAktualniProstor(sousedniProstor);
            return sousedniProstor.dlouhyPopis();
          } else {
            return "Nemáš věci pro přístup. Potřebuješ mapu.";
          }
        }
        /*if (sousedniProstor.equals(new Prostor("doky","doky. To je tvůj cíl."))){  //ZIMMIK existují i lepší způsoby jak zakázat přístup, pokud nemáš nějakou věc, al etohle je s prominutím pro retardy, takže to asi bude to co chcete, jenom bahca, ten prostor se musí přesně schodovat, jinak to nebude fungovat
             if (paluba.jeNaPalube("zlato")){
             hra.setKonecHry(true);
             return "Hra ukončena příchodem do doků.";
            }
            return "Či a či a čičičí. My jsme kluci kočičí.";
        }*/

        plan.setAktualniProstor(sousedniProstor);
        return sousedniProstor.dlouhyPopis();
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
