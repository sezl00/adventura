/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kodovani: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;

//ZIMMIK třída je celá nová, přepiš komentáře a nauč se co dělá

/**
 * Instance třídy PrikazVymen implementuje pro hru příkaz vyměň.
 *
 * @author Lukáš Sezima
 * @version rok 2017/2018
 *
 *
 **/
 class PrikazVymen implements IPrikaz {

    private static final String NAZEV = "vyměň";
    private HerniPlan plan;
    private Paluba paluba;

    /**
    *  Konstruktor třídy
    *
    *  @param plan herní plán, ve kterém se bude ve hře "ukazovat obsah toho, co máš na palubě"
    */
    public PrikazVymen(HerniPlan plan, Paluba paluba) {
        this.paluba = paluba;
        this.plan = plan;
    }


     @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0){
            return "Co mám vyměnit?";
        }

              if(plan.getAktualniProstor().equals(new Prostor("zátoka", "Jsi v oblasti Zde sídlí překupník. Smění s tebou ovoce za cukrovou třtinu."))) {
          if (parametry[0].equals("ovoce")) {       // v zátoce lze měnit pouze ovoce, které musí být na palubě
            if (paluba.jeNaPalube(parametry[0])) {
              paluba.odeber(parametry[0]);          //ovoce se odebere z paluby a vloží se cukrová třtina
              paluba.vloz(new Vec("cukrová_třtina", true));
              return "Dostal jsi cukrovou třtinu.";
            }
            return "Tohle nemáš v inventáři";
          }
          return "Tohle (" + parametry[0] + ") nelze zde vyměnit.";
        }

        if(plan.getAktualniProstor().equals(new Prostor("laguna","Bydlí zde starý námořník. Můžeš s ním směnit nějaké věci nebo si je u něj uschovat."))) {
          if (parametry[0].equals("ryby")) {         // v laguně lze měnit pouze ryby, které musí být na palubě
            if (paluba.jeNaPalube(parametry[0])) {
              paluba.odeber(parametry[0]);          //ryby se odeberou z paluby a vloží se mapa
              paluba.vloz(new Vec("mapa", true));
              return "Dostal jsi mapu.";
            }
            return "Tohle nemáš na palubě.";
          }
          return "Tohle (" + parametry[0] + ") nelze zde vyměnit.";
        }

        if(plan.getAktualniProstor().equals(new Prostor("mys","Bydlí zde starý námořník. Můžeš s ním směnit nějaké věci nebo si je u něj uschovat."))) {
          if (parametry[0].equals("cukrová_třtina")) {      // na mysu lze měnit pouze cukrovou třtinu, které musí být na palubě
            if (paluba.jeNaPalube(parametry[0])) {
              paluba.odeber(parametry[0]);                   //cukrová třtina se odebere z paluby a vloží se zlato
              paluba.vloz(new Vec("zlato", true));
              return "Dostal jsi zlato.";
            }
            return "Tohle nemáš na palubě.";
          }
          return "Tohle (" + parametry[0] + ") nelze zde vyměnit.";
        }
        
        return "Zde není možné měnit.";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
