/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kodovani: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;
import java.util.*;


/**
 * Instance třídy Paluba představuje prostor, který má omezenou kapacitu.
 *
 * @author Lukáš Sezima
 * @version rok 2017/2018
 */
public class Paluba
{
    private Collection<Vec> seznam;

    public Paluba()
    {
        seznam = new ArrayList<>();
    }
    /**Metoda pro vložení věci na palubu.
     * @param Věc nová věc
     * @return vrací false v případě, že věc není přenositelná
     * 
     */
    public boolean vloz(Vec vec) {
      if (!vec.isPrenositelnost())
        {return false;}
      if (seznam.size() >= 7) //maximální počet věcí v batohu je 7, ověření kapacity
       { return false;}
      return seznam.add(vec);
    }
    /**Metoda pro odebrání věci z batohu.
     * @param Název věci, která se má odebrat
     * @return NULL - vrací null, pokud je název věci neplatný
     * @return vec - vrátí věc, která byla odebrána
     * 
     */
    public Vec odeber(String nazev){ 
        for(Vec vec : seznam){
          if(vec.getNazev().equals(nazev)){
            seznam.remove(vec);
            return vec;
          }
        }
        return null;
    }
    /**Metoda, pro zobrazení seznamu věcí 
     * @return - vypíše obsah věcí, které jsou k dispozici
     */
    
    public Collection<Vec> vratSeznam(){
     return seznam;
    }
    /**Metoda - zjišťujeme, jestli jsou věci na palubě
     * @param - Názvy věcí
     * @return - vrací true v příůadě, že věc je na palubě, jinak false
     * 
     */
    public boolean jeNaPalube(String nazevVeci){ 
        for (Vec v : seznam){
          if (v.getNazev().equals(nazevVeci))
            {return true;}
        }
        return false;
    }
    /**Metoda - zjišťujeme, kolik věcí je na palubě
     * @return - vrací přesný počet
     * 
     */
    public String toString(){
       String velikost = "Počet věcí na palubě: ";
       int size = seznam.size();
       velikost += size;
       if(size == 0) {
           return velikost;
        }
       velikost += "\n";
       for (Vec vec : seznam){
           velikost =  velikost + vec.getNazev()  + " ";
       }
       return velikost;
    }
    /**Metoda pro počet věcí na palubě
     * @return - vrací velikost seznamu
     */
    public int pocet(){
        return seznam.size();
    }
    
}
