/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kodovani: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/**
 * Instance třídy PrikazUkaz implenetuje pro hru příkaz ukaž.
 *
 * @author Lukáš Sezima
 * @version rok 2017/2018
 *
 *
 **/
 class PrikazUkaz implements IPrikaz {

    private static final String NAZEV = "ukaž";
    private Paluba paluba;
    
    /**
    *  Konstruktor třídy
    *
    *  @param paluba, ukazuje slovní výčet toho, co je na palubě, tedy její obsah
    */
    public PrikazUkaz(Paluba paluba) {
        this.paluba=paluba;
    }


     @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0){ // ověření, že musím zadat, co chci ukázat
            return "Co mám ukázat?";
        }
        if (parametry[0].equals("paluba")) {
            return paluba.toString();
        }
        return "Promiň nevím, co mám ukázat.";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
