package logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Lukáš Sezima
 *@version    rok 2017/2018
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví přístaviště.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor přístaviště = new Prostor("přístaviště", "přístaviště. Odtud startuješ. Vezeš 7 prázdných beden.");
        Prostor moře = new Prostor("moře","moře. Tudy se pouze proplouvá.");
        Prostor zátoka = new Prostor("zátoka","zátoka. Zde sídlí překupník. Smění s tebou ovoce za cukrovou třtinu.");
        Prostor oceán = new Prostor("oceán","oceán. Zde můžeš ulovit ryby.");
        Prostor laguna = new Prostor("laguna","laguna. Potkal jsi starého námořníka. Můžeš od něj získat mapu, výměnou za bednu ryb.");
        Prostor průplav = new Prostor("průplav","průplav. Tudy pouze proplouváš.");
        Prostor doky = new Prostor("doky","doky. To je tvůj cíl.");
        Prostor neznámá_oblast = new Prostor("neznámá_oblast","neznámé oblasti. Zde jen proplouváš.");
        Prostor ostrov = new Prostor("ostrov","ostrov. Na ostrově si můžeš natrhat ovoce.");
        Prostor mys = new Prostor("mys","mys. Na mysu je obchodník. Smění s tebou zlato za cukrovou třtinu.");
        
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        přístaviště.setVychod(moře);
        moře.setVychod(přístaviště);
        moře.setVychod(zátoka);
        moře.setVychod(oceán);
        zátoka.setVychod(moře);
        oceán.setVychod(moře);
        oceán.setVychod(laguna);
        oceán.setVychod(průplav);
        oceán.setVychod(neznámá_oblast);
        laguna.setVychod(oceán);
        průplav.setVychod(oceán);
        průplav.setVychod(doky);
        doky.setVychod(průplav);
        neznámá_oblast.setVychod(oceán);
        neznámá_oblast.setVychod(mys);
        neznámá_oblast.setVychod(ostrov);
        mys.setVychod(neznámá_oblast);
        ostrov.setVychod(neznámá_oblast);
        
        //vytvoříme nové věci
        Vec ryby = new Vec("ryby",true);
        Vec ovoce = new Vec("ovoce",true);
        Vec potopena_lod = new Vec("potopená_loď",false);       
        
        //vytvořené věci přidáme do požadovaných prostorů
        oceán.vlozVec(ryby);
        ostrov.vlozVec(ovoce);
        neznámá_oblast.vlozVec(potopena_lod);
        aktualniProstor = přístaviště;  // hra začíná v přístavišti       
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

}
