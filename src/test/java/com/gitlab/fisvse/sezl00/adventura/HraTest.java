package com.gitlab.fisvse.sezl00.adventura;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author Lukáš Sezima
 * @version rok 2017/2018
 */
public class HraTest {
    private Hra hra1;

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     *
     */
    @Test
    public void testPrubehHry() {
        String jdiM = "jdi moře";
        String more = "moře";
        String jdiO = "jdi oceán";
        String ukažP = "ukaž paluba";
        String ocean = "oceán";
        String pruplav = "průplav";
        String jdiN = "jdi neznámá_oblast";
        String neznama_oblast = "neznámá_oblast";
        assertEquals("přístaviště", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiM);
        assertEquals(more, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi zátoka");
        assertEquals("zátoka", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiM);
        assertEquals(more, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals("Počet věcí na palubě: 0", hra1.zpracujPrikaz(ukažP));
        assertEquals("Sebral si ryby", hra1.zpracujPrikaz("seber ryby"));
        assertEquals("Počet věcí na palubě: 1\nryby ", hra1.zpracujPrikaz(ukažP));
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi laguna");
        assertEquals("Počet věcí na palubě: 1\nryby ", hra1.zpracujPrikaz(ukažP));
        assertEquals("Dostal jsi mapu.", hra1.zpracujPrikaz("vyměň ryby"));
        assertEquals("Počet věcí na palubě: 1\nmapa ", hra1.zpracujPrikaz(ukažP));
        assertEquals("laguna", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi mys");
        assertEquals("mys", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi ostrov");
        assertEquals("ostrov", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals("Nemáš věci pro přístup. Potřebuješ bednu s ovocem.", hra1.zpracujPrikaz("jdi průplav"));
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi ostrov");
        assertEquals("ostrov", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals("Počet věcí na palubě: 1\nmapa ", hra1.zpracujPrikaz(ukažP));
        assertEquals("Sebral si ovoce", hra1.zpracujPrikaz("seber ovoce"));
        assertEquals("Počet věcí na palubě: 2\nmapa ovoce ", hra1.zpracujPrikaz(ukažP));
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi průplav");
        assertEquals(pruplav, hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals(pruplav, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi doky");
        assertEquals(pruplav, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi ostrov");
        assertEquals("ostrov", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals("Počet věcí na palubě: 2\nmapa ovoce ", hra1.zpracujPrikaz(ukažP));
        assertEquals("Sebral si ovoce", hra1.zpracujPrikaz("seber ovoce"));
        assertEquals("Počet věcí na palubě: 3\nmapa ovoce ovoce ", hra1.zpracujPrikaz(ukažP));
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiM);
        assertEquals(more, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi zátoka");
        assertEquals("zátoka", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals("Počet věcí na palubě: 3\nmapa ovoce ovoce ", hra1.zpracujPrikaz(ukažP));
        assertEquals("Dostal jsi cukrovou třtinu.", hra1.zpracujPrikaz("vyměň ovoce"));
        assertEquals("Počet věcí na palubě: 3\nmapa ovoce cukrová_třtina ", hra1.zpracujPrikaz(ukažP));
        hra1.zpracujPrikaz(jdiM);
        assertEquals(more, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi mys");
        assertEquals("mys", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertEquals("Počet věcí na palubě: 3\nmapa ovoce cukrová_třtina ", hra1.zpracujPrikaz(ukažP));
        assertEquals("Dostal jsi zlato.", hra1.zpracujPrikaz("vyměň cukrová_třtina"));
        assertEquals("Počet věcí na palubě: 3\nmapa ovoce zlato ", hra1.zpracujPrikaz(ukažP));
        hra1.zpracujPrikaz(jdiN);
        assertEquals(neznama_oblast, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz(jdiO);
        assertEquals(ocean, hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi průplav");
        assertEquals(false, hra1.konecHry());
        assertEquals(pruplav, hra1.getHerniPlan().getAktualniProstor().getNazev());

        hra1.zpracujPrikaz("jdi doky");

        assertEquals(true, hra1.konecHry());
    }
}
