package com.gitlab.fisvse.sezl00.adventura;


import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author Lukáš Sezima
 * @version rok 2017/2018
 */
public class ProstorTest {

    /**
     * Testuje, zda jsou správně nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře.
     */
    @Test
    public void testLzeProjit() {
        String more = "Moře";
        String ocean = "Oceán";
        String neznama_oblast = "Neznámá_oblast";

        Prostor prostor1 = new Prostor("Přístaviště", "A");
        Prostor prostor2 = new Prostor("Moře", "B");
        Prostor prostor3 = new Prostor("Zátoka", "C");
        Prostor prostor4 = new Prostor("Oceán", "D");
        Prostor prostor5 = new Prostor("Laguna", "E");
        Prostor prostor6 = new Prostor("Neznámá_oblast", "F");
        Prostor prostor7 = new Prostor("Mys", "G");
        Prostor prostor8 = new Prostor("Ostrov", "H");
        Prostor prostor9 = new Prostor("Průplav", "I");
        Prostor prostor10 = new Prostor("Doky", "J");

        prostor1.setVychod(prostor2);

        prostor2.setVychod(prostor1);
        prostor2.setVychod(prostor3);
        prostor2.setVychod(prostor4);

        prostor3.setVychod(prostor2);

        prostor4.setVychod(prostor2);
        prostor4.setVychod(prostor5);
        prostor4.setVychod(prostor6);
        prostor4.setVychod(prostor9);

        prostor5.setVychod(prostor4);

        prostor6.setVychod(prostor4);
        prostor6.setVychod(prostor7);
        prostor6.setVychod(prostor8);

        prostor7.setVychod(prostor6);

        prostor8.setVychod(prostor6);

        prostor9.setVychod(prostor4);
        prostor9.setVychod(prostor10);

        prostor10.setVychod(prostor9);


        assertEquals(prostor2, prostor1.vratSousedniProstor(more));
        assertEquals(prostor1, prostor2.vratSousedniProstor("Přístaviště"));

        assertEquals(prostor3, prostor2.vratSousedniProstor("Zátoka"));
        assertEquals(prostor2, prostor3.vratSousedniProstor(more));

        assertEquals(prostor4, prostor2.vratSousedniProstor(ocean));
        assertEquals(prostor2, prostor4.vratSousedniProstor(more));

        assertEquals(prostor5, prostor4.vratSousedniProstor("Laguna"));
        assertEquals(prostor4, prostor5.vratSousedniProstor(ocean));

        assertEquals(prostor6, prostor4.vratSousedniProstor(neznama_oblast));
        assertEquals(prostor4, prostor6.vratSousedniProstor(ocean));

        assertEquals(prostor7, prostor6.vratSousedniProstor("Mys"));
        assertEquals(prostor6, prostor7.vratSousedniProstor(neznama_oblast));

        assertEquals(prostor8, prostor6.vratSousedniProstor("Ostrov"));
        assertEquals(prostor6, prostor8.vratSousedniProstor(neznama_oblast));

        assertEquals(prostor9, prostor4.vratSousedniProstor("Průplav"));
        assertEquals(prostor4, prostor9.vratSousedniProstor(ocean));

        assertEquals(prostor10, prostor9.vratSousedniProstor("Doky"));
        assertEquals(prostor9, prostor10.vratSousedniProstor("Průplav"));

    }

}
