/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kodovani: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package com.gitlab.fisvse.sezl00.adventura;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída PalubaTest slouží ke komplexnímu otestování třídy ...
 *
 * @author (jméno autora)
 * @version (číslo verze nebo datum)
 */
public class PalubaTest {
    private Paluba paluba;

    /**
     * Inicializace předcházející spuštění každého testu a připravující
     * tzv. přípravek (fixture), což je sada objektů, s nimiž budou testy
     * pracovat.
     */
    @Before
    public void setUp() {
        paluba = new Paluba();
    }


    // == Vlastní testy =============================================

    /**
     * Test pro ověření kapacity paluby. Přídávají a odebírají se jednotlivé věci.
     */
    @Test
    public void testKapacita() {
        Vec vec1 = new Vec("A", true);
        Vec vec2 = new Vec("B", true);
        Vec vec3 = new Vec("C", true);
        Vec vec4 = new Vec("D", true);
        Vec vec5 = new Vec("E", true);
        Vec vec6 = new Vec("F", true);
        Vec vec7 = new Vec("G", true);
        Vec vec8 = new Vec("H", false);


        assertEquals(true, paluba.vloz(vec1));
        assertEquals(false, paluba.vloz(vec8));
        assertEquals(vec1, paluba.odeber(vec1.getNazev()));
        assertEquals(null, paluba.odeber(vec8.getNazev()));
        assertEquals("Počet věcí na palubě: 0", paluba.toString());

        assertEquals(true, paluba.vloz(vec1));
        assertEquals(true, paluba.vloz(vec3));
        assertEquals(true, paluba.vloz(vec4));
        assertEquals(false, paluba.vloz(vec8));
        assertEquals(vec3, paluba.odeber(vec3.getNazev()));
        assertEquals(vec4, paluba.odeber(vec4.getNazev()));
        assertEquals("Počet věcí na palubě: 1\nA ", paluba.toString());

        assertEquals(vec1, paluba.odeber(vec1.getNazev()));
        assertEquals(true, paluba.vloz(vec2));
        assertEquals(true, paluba.vloz(vec4));
        assertEquals(true, paluba.vloz(vec6));
        assertEquals(true, paluba.vloz(vec7));
        assertEquals(vec6, paluba.odeber(vec6.getNazev()));
        assertEquals("Počet věcí na palubě: 3\nB D G ", paluba.toString());

        assertEquals(vec2, paluba.odeber(vec2.getNazev()));
        assertEquals(vec4, paluba.odeber(vec4.getNazev()));
        assertEquals(vec7, paluba.odeber(vec7.getNazev()));
        assertEquals(true, paluba.vloz(vec1));
        assertEquals(true, paluba.vloz(vec2));
        assertEquals(true, paluba.vloz(vec3));
        assertEquals(true, paluba.vloz(vec4));
        assertEquals(true, paluba.vloz(vec5));
        assertEquals(true, paluba.vloz(vec6));
        assertEquals(true, paluba.vloz(vec7));
        assertEquals("Počet věcí na palubě: 7\nA B C D E F G ", paluba.toString());
        assertEquals(false, paluba.vloz(vec3));
        assertEquals("Počet věcí na palubě: 7\nA B C D E F G ", paluba.toString());

    }

}
