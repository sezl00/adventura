package com.gitlab.fisvse.sezl00.adventura;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 *
 * @author Lukáš Sezima
 * @version rok 2017/2018
 */
public class SeznamPrikazuTest {
    public Hra hra;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazSeber prSeber;
    private PrikazVymen prVymen;
    private PrikazUkaz prUkaz;
    private PrikazNapoveda prNapoveda;
    public Paluba paluba;
    public HerniPlan herniPlan;
    private SeznamPrikazu seznPrikazu;

    @Before
    public void setUp() {
        seznPrikazu = new SeznamPrikazu();
        hra = new Hra();
        paluba = new Paluba();
        herniPlan = new HerniPlan();
        prJdi = new PrikazJdi(herniPlan, paluba, hra);
        prKonec = new PrikazKonec(hra);
        prSeber = new PrikazSeber(herniPlan, paluba);
        prVymen = new PrikazVymen(herniPlan, paluba);
        prUkaz = new PrikazUkaz(paluba);
        prNapoveda = new PrikazNapoveda(seznPrikazu);

    }

    /**
     * testování, jestli příkazy fungují
     */
    @Test
    public void testVlozeniVybrani() {

        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prVymen);
        seznPrikazu.vlozPrikaz(prUkaz);
        seznPrikazu.vlozPrikaz(prNapoveda);
        assertEquals(prKonec, seznPrikazu.vratPrikaz("konec"));
        assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        assertEquals(prSeber, seznPrikazu.vratPrikaz("seber"));
        assertEquals(prVymen, seznPrikazu.vratPrikaz("vyměň"));
        assertEquals(prUkaz, seznPrikazu.vratPrikaz("ukaž"));
        assertEquals(prNapoveda, seznPrikazu.vratPrikaz("nápověda"));
    }

    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("nápověda"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Konec"));
    }

    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals(true, nazvy.contains("konec"));
        assertEquals(true, nazvy.contains("jdi"));
        assertEquals(false, nazvy.contains("nápověda"));
        assertEquals(false, nazvy.contains("Konec"));
    }


}
